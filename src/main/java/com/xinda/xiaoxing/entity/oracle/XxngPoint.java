package com.xinda.xiaoxing.entity.oracle;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;


/**
 * <p>
 *
 * </p>
 *
 * @author betacat
 * @since 2021-04-19
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName("XXNG_POINT")
public class XxngPoint implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableField("ID")
    private String id;

    /**
     * 创建人
     */
    @TableField("CREATE_BY")
    private String createBy;

    /**
     * 创建日期
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;

    /**
     * 更新人
     */
    @TableField("UPDATE_BY")
    private String updateBy;

    /**
     * 更新日期
     */
    @TableField("UPDATE_TIME")
    private LocalDateTime updateTime;

    /**
     * 所属部门
     */
    @TableField("SYS_ORG_CODE")
    private String sysOrgCode;

    /**
     * 服务器
     */
    @TableField("SERVER")
    private String server;

    /**
     * 通道
     */
    @TableField("CHANNEL")
    private String channel;

    /**
     * 设备
     */
    @TableField("DEVICE")
    private String device;

    /**
     * 设备所在表
     */
    @TableField("DEVICE_TABLE")
    private String deviceTable;

    /**
     * 设备id
     */
    @TableField("DEVICE_ID")
    private String deviceId;

    /**
     * 设备名
     */
    @TableField("DEVICE_NAME")
    private String deviceName;

    /**
     * 线路
     */
    @TableField("LINE")
    private String line;

    /**
     * 点位
     */
    @TableField("ITEM")
    private String item;

    /**
     * 地址
     */
    @TableField("ADDRESS")
    private String address;

    /**
     * 数据类型
     */
    @TableField("DATA_TYPE")
    private String dataType;

    /**
     * 能源类型
     */
    @TableField("ENERGY_TYPE")
    private String energyType;

    /**
     * 启用
     */
    @TableField("ENABLE")
    private int enable;

    /**
     * 标记名
     */
    @TableField("TAG_NAME")
    private String tagName;

    /**
     * 描述
     */
    @TableField("DESCRIPTION")
    private String description;


}