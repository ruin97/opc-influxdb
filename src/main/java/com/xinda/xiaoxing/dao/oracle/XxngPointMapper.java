package com.xinda.xiaoxing.dao.oracle;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinda.xiaoxing.entity.oracle.XxngPoint;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author betacat
 * @since 2021-04-19
 */
public interface XxngPointMapper extends BaseMapper<XxngPoint> {

}
