//package com.xinda.xiaoxing.task;
//
//import com.xinda.xiaoxing.config.opc.ConnectionListener;
//import com.xinda.xiaoxing.service.OPCService;
//import org.openscada.opc.lib.da.Server;
//
//import java.util.Map;
//
//public class TaskThread implements Runnable{
//
//    private OPCService opcService;
//    private Server server;
//    private ConnectionListener connectionListener;
//    private String serverName;
//    private Map idItemMap;
//
//    public TaskThread() {
//    }
//
//    public TaskThread(OPCService opcService, Server server, ConnectionListener connectionListener, String serverName, Map idItemMap) {
//        this.opcService = opcService;
//        this.server = server;
//        this.connectionListener = connectionListener;
//        this.serverName = serverName;
//        this.idItemMap = idItemMap;
//    }
//
//    @Override
//    public void run() {
//        opcService.syncOPCData(server,connectionListener,serverName,idItemMap);
//    }
//}
