package com.xinda.xiaoxing.task;

import com.xinda.xiaoxing.condition.KEPCondition1;
import com.xinda.xiaoxing.config.opc.ConnectionListener;
import com.xinda.xiaoxing.service.OPCService;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.da.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Conditional;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.net.UnknownHostException;
import java.util.Map;

@Conditional(KEPCondition1.class)
@Component
@EnableScheduling
public class KEPSyncTask1 {

    @Autowired
    Server kepServer1;
    @Autowired
    ConnectionListener kepConnectionListener1;
    @Autowired
    Map<String, Item> kepIdItemMap1;
    @Autowired
    OPCService kepOPCService;

    /**
     * 初始化
     */
    @PostConstruct
    public void init() {
        try {
            kepServer1.connect();
        } catch (UnknownHostException | AlreadyConnectedException | JIException e) {
            e.printStackTrace();
        }
    }

    /**
     * 同步kep服务器数据
     */
    @Scheduled(cron = "*/10 * * * * ?")
    public void kepSync(){
        kepOPCService.syncOPCData(kepServer1,kepConnectionListener1,"kepServer1",kepIdItemMap1);
    }
}
